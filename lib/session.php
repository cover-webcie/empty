<?php

if (!defined('COVER_API_URL'))
	define('COVER_API_URL', 'https://www.svcover.nl/api');

if (!defined('COVER_COOKIE_NAME'))
	define('COVER_COOKIE_NAME', 'cover_session_id');
	
if (!defined('COVER_LOGIN_URL'))
	define('COVER_LOGIN_URL', 'https://www.svcover.nl/login');

if (!defined('COVER_LOGOUT_URL'))
	define('COVER_LOGOUT_URL', 'https://www.svcover.nl/logout');

/**
 * Build an URL from its parts. Reverse of parse_url().
 * @param array $parts the same parts as parse_url() yields
 * @return string constructed URL
 */
function http_build_url(array $parts)
{ 
	return implode("", array(
  		isset($parts['scheme']) ? $parts['scheme'] . '://' : '',
		isset($parts['user']) ? $parts['user'] : '',
		isset($parts['pass']) ? ':' . $parts['pass']  : '',
		(isset($parts['user']) || isset($parts['pass'])) ? "@" : '',
		isset($parts['host']) ? $parts['host'] : '',
		isset($parts['port']) ? ':' . intval($parts['port']) : '',
		isset($parts['path']) ? $parts['path'] : '',
		isset($parts['query']) ? '?' . $parts['query'] : '',
		isset($parts['fragment']) ? '#' . $parts['fragment'] : ''
	));
}

/**
 * Update the query parameters of an URL by parsing the URL and then merging the
 * values from $data.
 * @param string $url with optional query part
 * @param array $data as an associative array to be added to the query part of
 *        the url
 * @return string new url
 */
function http_inject_url($url, array $data)
{
	// Parse the url
	$url_parts = parse_url($url);

	// Explicitly parse the query part as well
	if (isset($url_parts['query']))
		parse_str($url_parts['query'], $url_query);
	else
		$url_query = array();

	// Splice in the token authentication
	$url_query = array_merge($data, $url_query);

	// Rebuild the url
	$url_parts['query'] = http_build_query($url_query);
	return http_build_url($url_parts);
}

/**
 * Perform a signed request to $url. Defaults to GET, but will do a POST request
 * if $post is not null.
 * @param $app string application identifier on the host (X-App header)
 * @param $secret string the secret that is used to derive the X-Hash header
 *        to sign the request.
 * @param $url string the URL
 * @param $post array an associative array that is submitted form-encoded as the
 *        body of the POST request. Setting this parameter to null will cause a
 *        GET request, otherwise a POST request will be performed.
 * @param $timeout int timeout of the request in seconds.
 */
function http_signed_request($app, $secret, $url, array $post = null, $timeout = 30)
{
	$body = $post !== null ? http_build_query($post) : '';

	$checksum = sha1($body . $secret);

	$headers = "X-App: $app\r\n"
	         . "X-Hash: $checksum\r\n";

	if ($post !== null)
		$options = [
			'header'  => $headers . "Content-type: application/x-www-form-urlencoded\r\n",
			'timeout' => $timeout,
			'method'  => 'POST',
			'content' => $body
	  	];
	else 
		$options = [
			'header'  => $headers,
			'timeout' => $timeout,
			'method'  => 'GET'
		];

	$context = stream_context_create(['http' => $options]);

	return file_get_contents($url, false, $context);
}

/**
 * Does a GET request to the url and signs it with the COVER_SECRET key.
 * This function requires COVER_APP and COVER_SECRET to be set.
 * @param string $url endpoint of the request
 * @param array $data optional extra parameters that are added to the url.
 * @return mixed response JSON parsed as an associative array.
 * @throws Exception when the host does not reply or when the response is
 *         not valid JSON.
 */ 
function http_get_json($url, array $data = null)
{	
	if ($data !== null)
		$url = http_inject_url($url, $data);

	$response = http_signed_request(COVER_APP, COVER_SECRET, $url);

	if (!$response)
		throw new Exception('No response');

	$data = json_decode($response, true);

	if ($data === null)
		throw new Exception('Response could not be parsed as JSON: ' . $response);

	return $data;
}

/**
 * Returns the current session a used is logged in with on the Cover domain.
 * This method depends on the COVER_COOKIE_NAME constant (and whether that
 * cookie is available, hence the Cover domain.)
 * @returns mixed an associative array with member data or null if there is no
 *          or an invalid session.
 */
function cover_session()
{
	static $session = null;

	// Is there a cover website global session id available?
	if (!empty($_COOKIE[COVER_COOKIE_NAME]))
		$session_id = $_COOKIE[COVER_COOKIE_NAME];

	// If not, bail out. I have no place else to look :(
	else
		return null;

	if ($session === null)
	{
		$data = array(
			'method' => 'session_get_member',
			'session_id' => $session_id
			);

		$response = http_get_json(COVER_API_URL, $data);

		$session = !empty($response['result'])
			? $response['result']
			: false;
	}

	return $session !== false ? $session : null;
}

/**
 * Test whether the current user is logged in with a valid session.
 * @return bool user has a valid session
 */
function cover_session_logged_in()
{
	return cover_session() !== null;
}

function _cover_session_url($url, $next_url = null, $next_field = 'referrer')
{
	if ($next_url === null)
		$next_url = $_SERVER['HTTP_PROTOCOL'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

	return http_inject_url($url, [$next_field => $next_url]);
}

/**
 * Link to login page. If you pass in a complete URL the user will be redirected
 * to that URL when the login is successful. Otherwise the current
 * url is used as the return page.
 * @param string $next_url 
 * @return string url where the user can log in
 */
function cover_login_url($next_url = null)
{
	return _cover_session_url(COVER_LOGIN_URL, $next_url);
}

/**
 * Link to logout page. If you pass in a complete URL the user will be
 * redirected to that URL when the logout is successful. Otherwise the current
 * url is used as the return page.
 * @param string $next_url 
 * @return string url where the user can log out
 */
function cover_logout_url($next_url = null)
{
	return _cover_session_url(COVER_LOGOUT_URL, $next_url);
}

/**
 * Get all committees a member is part of. Returns an associative array with
 * the internal committee names as key and the public committee names as value.
 * @return array committees or null if there is no active session.
 */
function cover_session_get_committees()
{
	$session = cover_session();
	return $session ? $session['committees'] : null;
}

/**
 * Test whether a member is part of a committee. Use the internal name. If
 * there is no active session, this function always returns false.
 * @param string $committee internal committee name to test for
 * @return bool whether the member is part of this committee.
 */
function cover_session_in_committee($committee)
{
	$committees = cover_session_get_committees();
	return $committees ? array_key_exists(strtolower($committee), $committees) : false;
}

/**
 * Get the full name of the currently logged in member. Returns null if there
 * is no active session. Does not take the privacy settings of a member into 
 * account, so only show this name to members themselves.
 * @return string the full name of a member or null if there is no active session.
 */
function cover_member_full_name()
{
	if ($member = cover_session())
		return implode(' ', array_filter([
			$member['voornaam'],
			$member['tussenvoegsel'],
			$member['achternaam']
		]));
	else
		return null;
}

/**
 * Get the first name of the currently logged in member. Returns null if there
 * is no active session. Does not take the privacy settings of a member into 
 * account, so only show this name to members themselves.
 * @return string the first name of a member or null if there is no active session.
 */
function cover_member_first_name()
{
	$member = cover_session();
	return $member ? $member['voornaam'] : null;
}