<?php

function add_entry($entry)
{
	if (strlen($entry) > 200)
		return false;

	$stmt = get_db()->prepare('UPDATE entries
	                           SET reported = reported + 1
	                           WHERE DATE(created_on) = DATE(NOW())
	                                 AND completed = 0
	                                 AND entry = :entry');

	$stmt->execute([':entry' => $entry]);

	if ($stmt->rowCount() > 0)
		return true;

	return get_db()
		->prepare('INSERT INTO entries (entry, created_on, completed) VALUES (:entry, NOW(), 0)')
		->execute([':entry' => $entry]);
}

function update_entry($entry_id, $completed)
{
	return get_db()
		->prepare('UPDATE entries SET completed = :completed WHERE id = :entry_id')
		->execute([
			':completed' => $completed ? 1 : 0,
			':entry_id' => $entry_id
		]);
}

function list_entries(DateTime $from = null, $show_completed = true)
{
	$conditions = [];
	$params = [];
	
	if ($from !== null) {
		$conditions[] = 'created_on > :from';
		$params[':from'] = $from->format('Y-m-d H:i:s');
	}

	if (!$show_completed) {
		$conditions[] = 'completed = 0';
	}

	$sql_conditions = implode(' AND ', $conditions);

	$stmt = get_db()
		->prepare("SELECT id, entry, created_on, completed, reported
			       FROM entries
			       WHERE $sql_conditions
			       ORDER BY created_on DESC");

	$stmt->execute($params);

	return $stmt->fetchAll(PDO::FETCH_OBJ);
}