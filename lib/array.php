<?php

function array_partition($array, $key_callback)
{
	$partitions = [];

	foreach ($array as $idx => $entry)
	{
		$key = $key_callback($entry, $idx);

		if (!isset($partitions[$key]))
			$partitions[$key] = [];

		$partitions[$key][] = $entry;
	}

	return $partitions;
}