<?php

function html_input($type, array $attributes = [])
{
	$attr_str = '';

	foreach ($attributes as $key => $value)
	{
		if ($value === false)
			continue;

		if ($value === true)
			$attr_str .= sprintf(' %s', $key);
		else
			$attr_str .= sprintf(' %s="%s"', $key, htmlspecialchars($value, ENT_QUOTES, 'utf-8'));
	}

	return sprintf('<input type="%s"%s>', $type, $attr_str);
}

function html_encode($str)
{
	return htmlspecialchars($str, ENT_HTML5, 'utf-8');
}