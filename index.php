<?php

require 'config.php';
require 'lib/session.php';
require 'lib/entries.php';
require 'lib/html.php';
require 'lib/array.php';

function get_db()
{
	static $db;

	if (!$db)
		$db = new PDO(PDO_DSN, PDO_USER, PDO_PASSWORD);

	return $db;
}

function main()
{
	if (cover_session_in_committee('board') || cover_session_in_committee('roomcee'))
		view_list();
	else
		view_form();
}

function view_list()
{
	if (isset($_POST['entry']))
		add_entry($_POST['entry']);

	if (isset($_POST['completed']))
		foreach ($_POST['completed'] as $id => $completed)
			update_entry($id, $completed);

	$show_completed = isset($_GET['completed']) && $_GET['completed'] == 'show';

	$entries = list_entries(new DateTime('-7 day'), $show_completed);

	$days = array_partition($entries, function($entry) {
		$date = new DateTime($entry->created_on);
		return $date->format('Y-m-d');
	});

	$uncompleted_count = array_reduce($entries, function($count, $entry) {
		return $entry->completed ? $count : $count + 1;
	}, 0);

	include 'tpl/list.phtml';
}

function view_form()
{
	$success = null;

	if (isset($_POST['entry']))
		$success = add_entry($_POST['entry']);

	include 'tpl/form.phtml';
}

main();
